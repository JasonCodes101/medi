/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import React, { Component } from 'react';
 import { AppRegistry, FlatList, NavigatorIOS, StyleSheet, Text, View } from 'react-native';
 import { Col, Row, Grid } from "react-native-easy-grid";
 import { Container, Header,Content, Footer, Left, Body, Right,FooterTab,Button, Icon, Title } from 'native-base';
 import HomeScreen from './home/index';
 import BuyScreen from './buy/index';
 import DocScreen from './doctors/index';
 import PlaceDataScreen from './placedata/index';
 import ItemDataScreen from './itemdata/index'

 import {
  StackNavigator,
} from 'react-navigation';

const Route = StackNavigator({
  Home: { screen: HomeScreen},
  Buy:  { screen: BuyScreen},
  Doctors: { screen: DocScreen},
  PlaceData: {screen: PlaceDataScreen},
  ItemData: {screen: ItemDataScreen}
},{headerMode: 'none'} );
 // import BuyScreen from './app/BuyScreen';
 //import { Router, Scene } from 'react-native-router-flux';


 export default class App extends Component {

   render() {
     return(  <Route /> )
   }
 }


AppRegistry.registerComponent('App', () => App);
