/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import React, { Component } from 'react';
 import { ListItem } from 'react-native-elements'
 import { AppRegistry, FlatList, StyleSheet, Text, View,ActivityIndicator } from 'react-native';
 import { Container, Header,Content, Footer,Left, Body, Right,FooterTab,Button, Icon,Title,List} from 'native-base';
 import { Col, Row, Grid } from "react-native-easy-grid";
 import {
  StackNavigator,
 } from 'react-navigation';



 export default class HomeScreen extends Component {

   constructor(props){
     super(props)
     this.state = {
       loading:true,
       responseData:null
     }
   }

     _keyExtractor = (item, index) => item.id;

   componentDidMount(){
     fetch('https://jasonthecoder.github.io/db.json')
      .then((response) => response.json())
      .then((responseJson)=>this.setState({responseData:responseJson,loading:false}))
      //  navigator.geolocation.getCurrentPosition(this.showPosition);

    // const { params } = this.props.navigation.state;
    // this.setState({stateData:params});

   }

   navigateRoute = (data) => {
    const { navigate } = this.props.navigation;
    navigate(data);
   }

   list = ({item}) =>{
     return (
       <ListItem
        onPress={() => this.props.navigation.navigate("ItemData",item)}
        title={item.key}
        subtitle={item.medi}
        containerStyle={{ borderTopWidth:0,borderBottomWidth: 1,backgroundColor: 'rgb(0, 191, 255)'}}
      />
     );
   }

   goBack(){
     this.props.navigation.goBack()
     this.setState({responseData:null});
   }

   render() {

     return (
       <Container style={styles.container}>
       <Header style={styles.Head}>
           <Left/>
           <Body>
             <Title style={{color:'#CCFFCC'}}>Medi</Title>
           </Body>
           <Right/>
         </Header>

         {this.state.responseData  &&  <Grid>
           <Col>
             <Text style={styles.titleStyle}>Medication</Text>
             {/* {this.listItemMedi(this.state.responseData)} */}
             <List>
                 <FlatList
                  data={this.state.responseData}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.list} />
            </List>
           </Col>
         </Grid>}
         {!this.state.responseData  &&<Grid><Col><ActivityIndicator animating size="large" style={{
           alignItems:'center',
           justifyContent:'center',
           paddingTop:50,
       }}/></Col></Grid> }
       <Text style={styles.important}>Pls visit a doctor for a healthy dosage of medication!</Text>
       <Footer style={styles.footer}>
          <FooterTab>
            <Button style={styles.borderstyle} onPress={this.navigateRoute.bind(this,'Buy')}  >
              <Text  style={{color:'#F3EFE0'}}>Find a Pharmacy</Text>
            </Button>
            <Button  onPress={this.navigateRoute.bind(this,'Doctors')}  >
              <Text style={{color:'#F3EFE0'}}>Find a Hospital</Text>
            </Button>
          </FooterTab>
        </Footer>
  </Container>
     );
   }
 }

 const styles = StyleSheet.create({
   item: {
     padding: 10,
     fontSize: 18,
     height: 44,
     marginTop: 20,
     color:'white'
   },
   footer:{
     backgroundColor:'#3399FF'
   },
   container:{
     backgroundColor:'rgb(0, 191, 255)'
   },
   Head:{
     marginTop:0,
     backgroundColor:'#003399'
   },
   important: {
     padding: 10,
     fontSize: 20,
     color:'white'
   },
   titleStyle:{
     padding: 10,
     fontSize: 20,
     color:'white',
     textAlign: 'center',
   },
   borderstyle:{
     borderRightWidth:1,
     borderRadius:0
   }

 })

AppRegistry.registerComponent('HomeScreen', () => HomeScreen);
