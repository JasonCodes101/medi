
import React, { Component } from 'react';
import { ListItem } from 'react-native-elements'
import { AppRegistry, FlatList, StyleSheet, Text, View,ActivityIndicator } from 'react-native';
import { Container, Header,Content, Footer,Left, Body, Right,FooterTab,Button, Icon, Title,List } from 'native-base';
import Loading from './loading';
import ImageLoad from 'react-native-image-placeholder';
import {
 StackNavigator,
} from 'react-navigation';
var x = null;
export default class Buy extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading:true,
      responseData:null,

    }
  }

   _keyExtractor = (item, index) => item.id;

   showPosition = (position) =>{
     var  lon =0;
     var  lat =0;

     lon = position.coords.longitude;
     lat = position.coords.latitude;
     x = `${lat},${lon}` ;
     console.log(x);
     //var x = "7.2657,79.8591";
     fetch(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${x}&radius=3000&type=pharmacy&key=AIzaSyD4FpSJmOSlmOnjMhQ5E7HaoF7O9Xib3Ok`)
      .then((response) => response.json())
      .then((responseJson)=>{
        console.log('responseJson',responseJson.results);
        this.setState({responseData:responseJson.results,loading:false})
      })

     //console.log('xx ',x);

     //Dear Sir Sharmal I tried Template Literals but i get a error saying lon is not defined.
    //  https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=7.2657,79.8591&radius=500&type=pharmacy&key=AIzaSyD4FpSJmOSlmOnjMhQ5E7HaoF7O9Xib3Ok
  }

  componentWillMount(){
    //navigator.geolocation.getCurrentPosition(this.showPosition);
  }

  componentDidMount(){

    //var x = "7.2657,79.8591";
    navigator.geolocation.getCurrentPosition(this.showPosition);

  }

  componentWillUnmount(){
    this.setState({responseData:false});
  }


  list = ({item}) =>{
    return (
      <ListItem roundAvatar
       onPress={() => this.props.navigation.navigate("PlaceData",item)}
       title={item.name}
       subtitle={`Rating: ${item.rating}`}
       avatar={{ uri: item.icon }}
       containerStyle={{ borderTopWidth:0,borderBottomWidth: 1,backgroundColor: '#fff' }}
     />
    );
  }
  goBack(){
    this.props.navigation.goBack()
    this.setState({responseData:null});
  }

  render() {

    console.log('this.state.responseData ',this.state.responseData);
    return (
      <Container style={styles.container}>
        <Header style={styles.Head}>
          <Left>
                    <Button transparent onPress={() => this.goBack()}>
                      <Icon style={{ color: "#fff", fontSize: 50 }} active name="ios-arrow-round-back-outline" />
                    </Button>
          </Left>
            <Body>
              <Title style={{ color: "#fff", fontSize: 14}}>Find a Pharmacy</Title>
            </Body>
            <Right/>
          </Header>
        {this.state.responseData  &&    <View>

            {/* {this.listItemMedi(this.state.responseData)} */}
        <List>
            <FlatList
             data={this.state.responseData}
             keyExtractor={this._keyExtractor}
             renderItem={this.list} />
       </List>


{/* <Text style={styles.item} key = {item.id}>{item.name}</Text> */}
        </View>}
        {/* {!this.state.responseData  &&<View><ActivityIndicator animating size="large" style={{
          alignItems:'center',
          justifyContent:'center',
          paddingTop:50,
          }}/></View> } */}
          {!this.state.responseData  &&<View>{<Loading/>}</View> }


       </Container>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    marginTop: 20,
    color:'white'
  },
  container:{

  },
  Head:{
    marginTop:0,
    backgroundColor:'rgb(0, 191, 255)'
  },
  important: {
    padding: 10,
    fontSize: 20,
    color:'white'
  },
  titleStyle:{
    padding: 10,
    fontSize: 20,
    color:'white'
  },
  borderstyle:{
    borderRightWidth:1,
    borderRadius:0
  }

})

AppRegistry.registerComponent('Buy', () => Buy);
