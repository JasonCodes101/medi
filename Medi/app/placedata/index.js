
import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View,Dimensions,ActivityIndicator,Image } from 'react-native';
import { Container, Header,Content, Footer,Left, Body, Right,FooterTab,Button, Icon, Title} from 'native-base';
import ImageLoad from 'react-native-image-placeholder';
import {
 StackNavigator,
} from 'react-navigation';
var {height, width} = Dimensions.get('window');
var newHeight = height/10 * 6;
export default class PlaceData extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading:true,
      responseData:null,
      stateData:null,
      photoData:null
    }
  }

   _keyExtractor = (item, index) => item.id;

  componentWillMount(){
    //navigator.geolocation.getCurrentPosition(this.showPosition);


  }

  componentDidMount(){
    const { params } = this.props.navigation.state;
    //console.log(params.photos);
  //  var photoUrl=null;
    if (params.photos){
      console.log("helloWorld");
      var URL = params.photos.map((data,k) =>{
        return data.photo_reference;
      });
        var photoUrl = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=${URL}&key=AIzaSyD4FpSJmOSlmOnjMhQ5E7HaoF7O9Xib3Ok`;
        //console.log('params ',photoUrl);
        this.setState({photoData:photoUrl});
    }

     this.setState({stateData:params});

  }

  render() {
     console.log(this.state.stateData);


    return (
      <Container style={styles.container}>
        <Header style={styles.Head}>
          <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                      <Icon style={{ color: "#fff", fontSize: 50 }} active name="ios-arrow-round-back-outline" />
                    </Button>
          </Left>
            <Body>
            {this.state.stateData &&  <Title style={{ color: "#fff", fontSize: 14 }}>{this.state.stateData.name}</Title>}
            </Body>
            <Right/>
          </Header>
       <View >
           {this.state.photoData &&
             <ImageLoad
                             style={{ width: width,
                                      height: newHeight }}
                             isShowActivity = {false}
                             source={{ uri: this.state.photoData }}/>
            }
        <View style={styles.info}>
           {this.state.stateData && <Text style={{color:'rgb(25, 25, 25)',fontSize:28}}>{this.state.stateData.name}</Text>}
           {this.state.stateData && this.state.stateData.opening_hours && this.state.stateData.opening_hours.open_now && <Text style={{fontSize:20,color:'green'}}>Open Now</Text>}
           {this.state.stateData && this.state.stateData.opening_hours && !this.state.stateData.opening_hours.open_now && <Text style={{fontSize:20,color:'red'}}>Closed Now</Text>}
           {this.state.stateData && this.state.stateData.vicinity && <Text style={{color:'rgb(25, 25, 25)',fontSize:18}}>Address:{this.state.stateData.vicinity}</Text>}
        </View>

        </View>


       </Container>
    );
  }
}

const styles = StyleSheet.create({
  info:{
    padding:12
  },
  container:{
     backgroundColor:'white'
  },
  Head:{
    marginTop:0,
    backgroundColor:'rgb(0, 191, 255)'
  },
  important: {
    padding: 10,
    fontSize: 20,
    color:'white'
  },
  titleStyle:{
    padding: 10,
    fontSize: 20,
    color:'white'
  },
  borderstyle:{
    borderRightWidth:1,
    borderRadius:0
  }

})

AppRegistry.registerComponent('PlaceData', () => PlaceData);
