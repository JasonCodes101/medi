
import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View,Dimensions,ActivityIndicator,Image } from 'react-native';
import { Container, Header,Content, Footer,Left, Body, Right,FooterTab,Button, Icon, Title} from 'native-base';
import ImageLoad from 'react-native-image-placeholder';
import {
 StackNavigator,
} from 'react-navigation';
var {height, width} = Dimensions.get('window');
var newHeight = height/10 * 6;

export default class ItemData extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading:true,
      responseData:null,
      stateData:null
    }
  }

  componentDidMount(){

     //  navigator.geolocation.getCurrentPosition(this.showPosition);

   const { params } = this.props.navigation.state;
   this.setState({stateData:params});
   console.log(params);

  }


  render() {
    return (
      <Container style={styles.container}>
        <Header style={styles.Head}>
          <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                      <Icon style={{ color: "#fff", fontSize: 50 }} active name="ios-arrow-round-back-outline" />
                    </Button>
          </Left>
            <Body>
            {this.state.stateData &&  <Title style={{ color: "#fff", fontSize: 14 }}>{this.state.stateData.key}</Title>}
            </Body>
            <Right/>
          </Header>
            {this.state.stateData &&  <ImageLoad
                             style={{ width: width,
                                      height: newHeight }}
                             isShowActivity = {false}
                             source={{ uri: this.state.stateData.mediImage }}/>
            }
            {this.state.stateData &&  <Text style={{color:'rgb(25, 25, 25)',fontSize:28}}>{this.state.stateData.medi}</Text> }
      </Container>


    );
  }
}

const styles = StyleSheet.create({
  info:{
    padding:12
  },
  container:{
     backgroundColor:'white'
  },
  Head:{
    marginTop:0,
    backgroundColor:'rgb(0, 191, 255)'
  },
  important: {
    padding: 10,
    fontSize: 20,
    color:'white'
  },
  titleStyle:{
    padding: 10,
    fontSize: 20,
    color:'white'
  },
  borderstyle:{
    borderRightWidth:1,
    borderRadius:0
  }

})


// skip this line if using Create React Native App
AppRegistry.registerComponent('ItemData', () => ItemData);
