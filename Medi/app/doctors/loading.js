import React, { Component } from "react";
import { TouchableOpacity, Dimensions ,Animated,Easing ,StyleSheet} from "react-native";
import { Container, Content, Icon, View, Text, List, ListItem, Thumbnail, Left, Right, Body, Header, Button, Title} from "native-base";
var { height, width } = Dimensions.get('window');


const styles = StyleSheet.create({
  loaderCircle : { width: 50, height: 50, backgroundColor: "#01579B55", borderRadius: 75 / 2 },
  loaderLineOne : { height: 10, width: 200, backgroundColor: "#01579B22", borderRadius: 5 },
  loaderLineTwo : { height: 5, width: 140, backgroundColor: "#01579B22", borderRadius: 5, marginTop: 6 },
  loaderLineThree : { height: 4, width: 60, backgroundColor: "#01579B22", borderRadius: 5, marginTop: 6 }
})
class Loading extends Component {


  constructor(props) {
    super(props);
    this.state = {
     animatedValue : new Animated.Value(0.2)
    }
  }

  componentDidMount = () =>  {
    this.startAnimating();
  }

  startAnimating = () =>{
    Animated.timing( this.state.animatedValue ,{
      toValue : 0.9,
      duration : 1500,
      easing : Easing.ease
    }).start(() => this.startAnimating());
  }


  render() {

    const  animationStyle = { opacity : this.state.animatedValue }

    const item = <ListItem avatar style={{marginTop:5}}>
      <Left>
        <Animated.View style={[styles.loaderCircle , animationStyle ]} />
      </Left>
      <Body>
        <View style={{ marginTop:5 }}>
          <Animated.View style={[styles.loaderLineOne , animationStyle]}/>
          <Animated.View style={[styles.loaderLineTwo , animationStyle]}/>
           <Animated.View style={[styles.loaderLineThree , animationStyle]}/>
        </View>
      </Body>
    </ListItem>
    return (
      <View style={{ height: height }}>
        <List>
          {item}
          {item}
          {item}
          {item}
          {item}
        </List>
      </View>
    );
  }
}

export default Loading;
